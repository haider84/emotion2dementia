close all
clear all
clc
% % load('eGeMapsDementiaBank')
% % load('emoDBSVMModel.mat')
% % 
% % [precc, ~, ~,scorecc]= predict(MdlQuadratic,datacc);
% % [precd,~,~, scorecd]= predict(MdlQuadratic,datacd);
% % 
% % Ccc = categorical(precc,[1 2 3 4 5 6 7],{'Anger','Boredom','Disgust','Fear','Happiness','Sadness','Neutral'})
% % Ccd = categorical(precd,[1 2 3 4 5 6 7],{'Anger','Boredom','Disgust','Fear','Happiness','Sadness','Neutral'})
% % 
% % figure;
% % 
% % histogram(Ccc,'BarWidth',0.5)
% % hold on
% % histogram(Ccd,'BarWidth',0.5)
% % legend('nonAD','AD')
% % ylabel('Number of Speech Segments')
% % datacc=scorecc;
% % datacd=scorecd;
for classifier=1:1:5

load('eGeMapsDementiaBank')
clear datacc datacd
load('score.mat')
% load('emobaseADRSomC5.mat')
% data=dataSOM';

Tcc = table( ccSegments', datacc)

Tcd = table( cdSegments', datacd)
segments=[ccSegments cdSegments];
 data=[datacc ;datacd];
% [Lambda,Psi,T,stats,data] = factoran(dataf,5,'scores','regression');
% clear net segments

duration=[durationcc durationcd];
label=[zeros(1,length(ccSegments)), ones(1, length(cdSegments))];


load('ageGender.mat')


for i=1:1:length(ccSegments)
   
    filesSegcc{i}=ccSegments{i}(1:9);
    
end

for i=1:1:length(cdSegments)
   
    filesSegcd{i}=cdSegments{i}(1:9);
    
end

filesSeg=[filesSegcc filesSegcd];

files=[unique(filesSegcc)'; unique(filesSegcd)'];
clear i 


for i=1:1:length(files)
    k=1;
    for j=1:1:length(filesSeg)
   
    tf = strcmp(files{i},filesSeg{j});
    
    if(tf==1)
        %temp(k,:)=[ age(i)' gender(i)' data(j,:)];
        temp(k,:)=[  data(j,:)];
        templ(k)=label(j);
        tempd(k)=duration(j);
        k=k+1;
    end
    
    end
    dataSubject{i}=temp';
    labelSubject{i}=templ';
    durationSubject{i}=tempd';
    clear temp templ tempd
    clear k
end

dataSubject=dataSubject';



labelSubject= labelSubject';
durationSubject=durationSubject';
s=1; 



%labels=[zeros(1,82) ones(1,82)];



tempLabels= labelSubject;
tempData=dataSubject;
tempDuration=durationSubject;
for j=1:1:length(dataSubject)
    
    
   
    tempData(j)=[];
    tempLabels(j)=[];

    
    testDataVec=dataSubject{j};
    testDuration=durationSubject{j};
    
    
    trainDataVec=tempData;
    trainLabelsVec=tempLabels;
    
    
   
    tempData=dataSubject;
    tempLabels= labelSubject;
    tempDuration=durationSubject;

    testData=testDataVec';
    idSubjectVec{j}=j.*ones(1,size(testData,1));
    
    trainData=cell2mat(trainDataVec')';
    trainLabels=cell2mat(trainLabelsVec);
   
    t = templateSVM('Standardize',1);
    rng('default'); % For reproducibility

    
    
%%

switch classifier
    case 1
        MdlQuadratic = fitcdiscr(trainData,trainLabels,'discrimType','pseudoLinear');
        [pre, score]= predict(MdlQuadratic,testData);
    case 2
        MdlQuadratic = fitcsvm(trainData,trainLabels,'Standardize',true,'KernelFunction','Linear','KernelScale','auto','Solver','SMO','BoxConstraint',0.1);
        [pre, score]= predict(MdlQuadratic,testData);
    case 3
        MdlQuadratic = fitctree(trainData,trainLabels,'MinLeafSize',20);
        [pre, score]= predict(MdlQuadratic,testData);
    case 4
        MdlQuadratic = fitcknn(trainData,trainLabels);
        [pre, score]= predict(MdlQuadratic,testData);
    case 5
        MdlQuadratic = TreeBagger(50,trainData,trainLabels,'MinLeafSize',20);
        [pre, score]= predict(MdlQuadratic,testData);
        pre=str2num(cell2mat(pre));
%      case 6
%          load('emoDBSVMModel.mat')
%          [pre, score]= predict(MdlQuadratic,testData);
end




% Create a Pattern Recognition Network
%hiddenLayerSize = 50;
%net = patternnet(hiddenLayerSize);
% Set up Division of Data for Training, Validation, Testing
%net.divideParam.trainRatio = 100/100;
%net.divideParam.valRatio = 0/100;
%net.divideParam.testRatio = 0/100;




% Train the Network
%[net,tr] = train(net,trainData',trainLabels');


% Test the Network
%a= net(testData');

%for i=1:1:length(a)
%if a(i)> 0.5
 %   pre(i)=0;
%end

%if a(i)<0.5
%   pre(i)=1;
%end
    
%end

%% for RF
% pre=str2num(cell2mat(pre));

%% Duration Vote
% a= find(pre==0);
% b=find(pre==1);
% 
% cc=sum(testDuration(a));
% 
% cd=sum(testDuration(b));
% 
% scoreDurationVote(j)=cd/(cc+cd);
% 
% 
% 
% labelPre(j)=0;
% 
% 
% if(cc<cd)
% 
%  labelPre(j)=1;  
% end

%% majority vote
a= length(find(pre==0));
b=length(find(pre==1));

scoreSegmentVote(j)=b/(a+b);
labelPre(j)=mode(pre);

%%
labelPreVec{j}=pre;
j;
clear pre score  trainData trainLabels net a b
end
%% at Segment Level
idSubject=cell2mat(idSubjectVec);
labelPreS=cell2mat(labelPreVec');
CnS = confusionmat(label,labelPreS')
M=[idSubject' label' duration' labelPreS];
accuracyS=(CnS(1,1)+CnS(2,2))./sum(sum(CnS))
% dlmwrite('resultsLDASegments.csv',M,',')
% save('resultsLDASegments.mat','label','labelPreS')

%% at subject Level
labels=[zeros(1,82) ones(1,82)];
Cn = confusionmat(labels,labelPre')

%%
accuracy=(Cn(1,1)+Cn(2,2))./sum(sum(Cn))

% save('resultsDTSegmentsMajorityVote.mat','labels','labelPre','scoreSegmentVote')
%  save('resultsRFSegmentsDurationVote.mat','labels','labelPre','scoreDurationVote')
switch classifier
    case 1
        save('./results/resultsLDASegments.mat','label','labelPreS')
        save('./results/resultsLDAMV.mat','labels','labelPre')
    case 2
        save('./results/resultsSVMSegments.mat','label','labelPreS')
        save('./results/resultsSVMMV.mat','labels','labelPre')
        
    case 3
        save('./results/resultsDTSegments.mat','label','labelPreS')
        save('./results/resultsDTMV.mat','labels','labelPre')        
        
        
    case 4
        save('./results/resultsKNNSegments.mat','label','labelPreS')
        save('./results/resultsKNNMV.mat','labels','labelPre')        
        
     case 5
        save('./results/resultsRFSegments.mat','label','labelPreS')
        save('./results/resultsRFMV.mat','labels','labelPre')       
        
end
clear all
end