close all
clear all
clc
load('eGeMapsDementiaBank')
load('emoDBSVMModel.mat')

[precc, ~, ~,scorecc]= predict(MdlQuadratic,datacc);
[precd,~,~, scorecd]= predict(MdlQuadratic,datacd);

Ccc = categorical(precc,[1 2 3 4 5 6 7],{'Anger','Boredom','Disgust','Fear','Happiness','Sadness','Neutral'})
Ccd = categorical(precd,[1 2 3 4 5 6 7],{'Anger','Boredom','Disgust','Fear','Happiness','Sadness','Neutral'})

y=histogram(Ccc,'BarWidth',0.5)
y=y.Values;
z1=histogram(Ccd,'BarWidth',0.5)
xl=categorical(z1.Categories);
z=z1.Values;


figure;

b=bar(xl,[y; z]')
xtips1 = b(1).XEndPoints;
ytips1 = b(1).YEndPoints;
labels1 = string(b(1).YData);
text(xtips1,ytips1,labels1,'HorizontalAlignment','center',...
    'VerticalAlignment','bottom')
xtips2 = b(2).XEndPoints;
ytips2 = b(2).YEndPoints;
labels2 = string(b(2).YData);
text(xtips2,ytips2,labels2,'HorizontalAlignment','center',...
    'VerticalAlignment','bottom')

legend('nonAD','AD')
xlabel('Emotion/Affect')
ylabel('Number of Speech Segments')



datacc=scorecc;
datacd=scorecd;


segmentst=[ccSegments cdSegments];
% datat=[datacc ;datacd];
duration=[datacc ;datacd];


% ax=[25 53 47];

load('ageGender.mat')

ii=1;



labelcc=zeros(1,82);
labelcd=ones(1,82);
labels=[labelcc labelcd];

for i=1:1:length(ccSegments)

    filesSegcc{i}=ccSegments{i}(1:9);

end

for i=1:1:length(cdSegments)

     filesSegcd{i}=cdSegments{i}(1:9);

end


files=[unique(filesSegcc)' ;unique(filesSegcd)'];
filesSeg=[filesSegcc filesSegcd];

clear i
outputs = [precc;precd];



for k=1:1:length(files)
b=find(strcmp(filesSeg, files(k)));
eachAudioFileTrain=outputs(b);
audioClusterStatesTrain{k}=eachAudioFileTrain;
eachAudioFileTrainDuration=duration(b);
audioClusterTrainDuration{k}=eachAudioFileTrainDuration;
end

for i=1:1:length(audioClusterStatesTrain)
    fileData=audioClusterStatesTrain{i}';
    fileDuration=audioClusterTrainDuration{i}';
    for j=1:1:7 % 6+1 emotions
    ADR(i,j)=length(find(fileData==j));
    dADR(i,j)=sum(fileDuration(find(fileData==j)));
    
    if(fileDuration(find(fileData==j))>0) % avoiding zeros 
    IADR(i,j)=entropy(fileDuration(find(fileData==j))); % calculating entropy
    end

    end
    vADR(i)=mean(diff(fileData));
    sADR(i)=std(fileData);
    vsADR(i)=std(diff(fileData));
    asADR(i)=std(diff(vADR(i)));
    aADR(i)=mean(diff(vADR(i)));
    clear fileData fileDuration
end

temporal=[ vADR; vsADR ]';
for i=1:1:length(ADR)
    nADR(i,:)=ADR(i,:)./sum(ADR(i,:));
    dnADR(i,:)=dADR(i,:)./sum(dADR(i,:));
%     fas(i)=sum(ADR(i,:));./sum(ADR(i,:))
end


% for i=1:1:length(dnADR)
%    IdnADR(i,:)=entropy(nADR(i,:)); 
% end


%% cross validation 
clear k j

j=1:1:164;

for k=1:1:164

%dataADR=[nADR ADR dADR dnADR];
%trainADR=[age' gender' nADR dnADR temporal];age' gender'
trainADR=[ IADR ];
trainLabels=labels';
trainADR(k,:)=[];
trainLabels(k)=[];
%valADR=[age(k)' gender(k)' nADR(k,:) dnADR(k,:) temporal(k,:)];age(k)' gender(k)'
valADR=[ IADR(k,:)];


valLabels=labels(k);
% t = templateSVM('Standardize',1);

% MdlQuadratic = fitcdiscr(trainADR,trainLabels,'discrimType','pseudoLinear');
% MdlQuadratic = fitcsvm(trainADR,trainLabels,'Standardize',true,'KernelFunction','Linear','KernelScale','auto','Solver','SMO','BoxConstraint',0.1);
% MdlQuadratic = fitcknn(trainADR,trainLabels');
MdlQuadratic = fitctree(trainADR,trainLabels,'MinLeafSize',20);
% MdlQuadratic = fitcnb(trainADR,trainLabels,'DistributionNames','kernel');
% figure;
% view(MdlQuadratic,'Mode','graph');
% view(MdlQuadratic);
% MdlQuadratic = fitcknn(trainADR,trainLabels);
% MdlQuadratic = TreeBagger(150,trainADR,trainLabels,'MinLeafSize',20);
% MdlQuadratic =fitcecoc(trainADR,trainLabels,'Learners',t);
[pre, score]= predict(MdlQuadratic,valADR);


pre=str2num(cell2mat(pre));
l(k)=pre;
s(k,:)=score;

end
ll{ii}=l;
Cn = confusionmat(labels,ll{ii})
prediction=ll{ii}';
age=age';
gender=gender';
target=labels';
%T = table(files,age,gender,nADR,dnADR,temporal, target,prediction)
%writetable(T,'compare_DT.txt','Delimiter',',')
accuracy(ii)=(Cn(1,1)+Cn(2,2))./sum(sum(Cn))
% ii=ii+1;
% clear ADR nADR k i net dADR dnADR temporal l trainADR
%
% save('./results/ADR/mrcg/resultsDTADR.mat','ll','labels','accuracy')

% save('./ABR/resultsRFABR.mat','ll','labels','accuracy')