close all
clear all
clc


load('resultsRFABR.mat')
RFABR=ll{1,1};
clear accuracy ll
load('resultsKNNMV.mat')
KNN=labelPre;
clear labelPre
load('resultsRFMV.mat')
RF=labelPre;
clear labelPre
 f=[KNN; RF ; RFABR];
 fl=zeros(1,164);

for i=1:1:length(labels)
   
    if(sum(f(:,i))>1)
        fl(i)=1;
    end
   
    
end

Cn = confusionmat(labels,fl)
accuracy=(Cn(1,1)+Cn(2,2))./sum(sum(Cn))

confusionchart(Cn)

load('fJSTSP.mat')
fpre=[fl; fJSTSP'];





 fFinal=zeros(1,164);

for i=1:1:length(labels)
   
    if(sum(fpre(:,i))>.5)
        fFinal(i)=1;
    end
   
    
end

Cnf = confusionmat(labels,fFinal)
accuracyf=(Cnf(1,1)+Cnf(2,2))./sum(sum(Cnf))