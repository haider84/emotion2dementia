close all
clear all
clc
load('sel.mat')
load('matlab.mat')
clear data;
data= csvread('egemapsEmoDB.csv',0,1);
data=data(:,f);


for i=1:1:length(files)

spkID(i)=str2num(files{i}(1:2));
labelStr{i}=files{i}(6:6);

end

for i=1:1:length(labelStr)
    
    t1 = strcmp(labelStr(i),'W'); % anger
    t2 = strcmp(labelStr(i),'L'); % boredom
    t3 = strcmp(labelStr(i),'E'); % disgust
    t4 = strcmp(labelStr(i),'A'); % anxiety/fear
    t5 = strcmp(labelStr(i),'F'); % happiness
    t6 = strcmp(labelStr(i),'T'); % sadness
    t7 = strcmp(labelStr(i),'N'); % neutral version
    
    if(t1==1)
        label(i)=1;
    end
    
    if(t2==1)
        label(i)=2;
    end
    
    if(t3==1)
        label(i)=3;
    end
    
    
    if(t4==1)
        label(i)=4;
    end
    
    
    if(t5==1)
        label(i)=5;
    end
    
        
    if(t6==1)
        label(i)=6;
    end
        
 
    if(t7==1)
        label(i)=7;
    end
        
    
    clear t1 t2 t3 t4 t5 t6 t7
    
end


subjects=unique(spkID);

for j=1:1:length(subjects)
    
    a=find(spkID==subjects(j));
    b=find(spkID~=subjects(j));
    
    
    valData=data(a,:);
    valLabel=label(a);
    
    trainData=data(b,:);
    trainLabel=label(b);

t = templateSVM('Standardize',true,'KernelFunction','Linear','KernelScale','auto','Solver','SMO','BoxConstraint',1.15);
rng('default'); % For reproducibility   
% MdlQuadratic = fitcdiscr(trainData,trainLabel,'discrimType','pseudoLinear');

MdlQuadratic =fitcecoc(trainData,trainLabel,'Learners',t,'FitPosterior',true);
% [ScoreCVSVMModel,ScoreParameters] = fitSVMPosterior(MdlQuadratic);
% MdlQuadratic = fitcsvm(trainData,trainLabel,'Standardize',true,'KernelFunction','Linear','KernelScale','auto','Solver','SMO');

% MdlQuadratic = fitctree(trainData,trainLabel,'MinLeafSize',20);
% MdlQuadratic = fitcknn(trainData,trainLabel);
% MdlQuadratic = TreeBagger(1500,trainData,trainLabel,'MinLeafSize',5);
% [label,~,~,Posterior] = resubPredict(MdlQuadratic,'Verbose',1);

[pre,~ ,~,score]= predict(MdlQuadratic, valData);
%   pre=str2num(cell2mat(pre));   
labelPreVec{j}=pre;
labelVec{j}=valLabel';
    
    
end

labelPre=cell2mat(labelPreVec');
labelTrue=cell2mat(labelVec');
Cn = confusionmat(labelTrue,labelPre)
accuracy=(Cn(1,1)+Cn(2,2)+Cn(3,3)+Cn(4,4)+Cn(5,5)+Cn(6,6)+Cn(7,7))./sum(sum(Cn))
UAR=mean(recall(Cn))